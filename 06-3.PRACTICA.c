/*
 * Realizar un programa que permita la carga de valores enteros por teclado. 
 * Luego de ingresar el valor mostrar un mensaje por pantalla que pida confirmar 
 * al usuario si desea cargar otro valor ingresando los caracteres 'S' o 'N'. 
 * Mostrar al final el valor "minimo" y el valor "maximo" de los valores ingresados.
 * Solo se pueden utilizar las siguientes funciones: 
 *      printf
 *      scanf
 *      for/while/do while
 *
 */

#include <stdio.h>

int main(){

    int valores;
    int minimo;
    int maximo;
    char continuar;
    int i = 0;
   
   
    for( ; ; ){
        
        printf("Ingrese un valor: ");
        scanf(" %d", &valores);
    
        printf("Desea ingresar otro valor S/N): ");
        scanf(" %c",&continuar);
        
        if(i == 0){
        maximo = valores;
        minimo = valores;
        } else{
        if(valores > maximo){
            maximo = valores;
        } if(valores < minimo){
            minimo = valores;
        }
        }
        
        i = i +1;
        if(continuar == 'N'){
            break;
        }
        
   }

        printf("El minimo es: %d \n",minimo);
        printf("El maximo total es: %d \n",maximo);

        
    return 0;
}