#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

void myHandler(int sig);

int main()
{
    
    signal(SIGTRAP,SIG_IGN);
    signal(SIGBUS,&myHandler);
    
    while(1)
    {
        printf("Hola mundo\n");
        sleep(3);
    }

 
    return 0;
}
void myHandler(int sig)
{
    printf("El numero de la señal es: %d\n",sig);
    
}



