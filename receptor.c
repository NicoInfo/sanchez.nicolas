#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <signal.h>

void my_handler(int sig);
int main()
{
    int fd;
    char buf[10];
    FILE* fp_bin;
    
    fd = open("./PinPumPam.txt",O_RDONLY);
    
    read(fd,buf,sizeof(buf));
    
    printf("El valor del buf es %s\n",buf);
    
    close(fd);
    
    fp_bin = fopen("resultados.bin","wb-ab");
    
    fseek(fp_bin,0,SEEK_SET);
    
    fwrite(&buf,sizeof(buf),1,fp_bin);
    
    fclose(fp_bin);
    
    while(1)
    {
     
        signal(SIGINT,&my_handler);
        sleep(1);
    }
    
    
    //printf("%s\n",buf);
    
 
    return 0;
}
void my_handler(int sig)
{
    printf("Cerrando la comunicacion\n");
    exit(0);
}
