#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>


struct part_number
{
    int id;
    char type;
    char description[80];
    char sku[15];
    int stock;
};
typedef struct part_number PART_NUMBER;

int getPartNumberByld(FILE* database,int id,PART_NUMBER **pAElemento);

char* typeNumberToTypeWord(char type,char component[]);
void myHandler(int sig);
//void myHandler2(int cantidad_buscada);
int struct2json(PART_NUMBER *elemento,char json[]);
int control_argumentos(int argc,char argv[]);
void menu_interativo(char argv[]);

int main(int argc,char **argv)
{

   int valor_return;
   valor_return = control_argumentos(argc,argv[1]);
   if(valor_return == -1 || valor_return == -2)
   {
       exit(0);
   }
       
    while(1)
    {
    
        menu_interativo(argv[1]);       
    
        signal(SIGINT,&myHandler);
        //signal(SIGUSR1,&myHandler2(cantidad_buscada));
        sleep(1);
    }
    
    return 0;
}
int getPartNumberByld(FILE* database,int id,PART_NUMBER **pAElemento)
{

    PART_NUMBER aux_struct;
    int i = 0;
    int valor_retorno;
    fseek(database,0,SEEK_SET);
    char json[1000];
    char string_id[50];
    char string_stock[50];
    char component[20];
    char *posicion_string;
    int cant_caracter_escrito = 0;

        while((fread(&aux_struct,sizeof(PART_NUMBER),1,database))!=0)
        {
        
        if(aux_struct.id == id)
            {

                
                
                    memset(&json,0,sizeof(json));
                    
                    pAElemento[i]=(PART_NUMBER*)malloc(sizeof(PART_NUMBER));
                    
                    if(pAElemento[i]==NULL)
                    {
                        printf("No se pudo reservar memoria\n");
                        valor_retorno = -1;
                        exit(0);
                    }
                    *pAElemento[i] = aux_struct;
                    
                    cant_caracter_escrito = struct2json(pAElemento[i], json);
                    printf("%s",json);
                    printf("\n");
                    printf("La cantidad de caracteres son: %d\n",cant_caracter_escrito);
                    printf("************************************\n");
              
             i++;
             valor_retorno = 0;
             break;

            }else
            {
                valor_retorno = 1; 
            }
        }
   
    return valor_retorno;
}
char* typeNumberToTypeWord(char type,char component[20])
{
    //printf("Estas en typeNumberToTypeWord\n");
    //printf("El type es: %c\n",type);
   
    char *valor_retorno;
    switch(type)
    {
        case 'R':
            printf("componente resistor\n");
            
            strcpy(component,"resistor");
            break;
            
        case 'C':
            strcpy(component,"capacitor");
            break;
        case 'I':
            strcpy(component,"inductor");
            break;
        case 'D':
            strcpy(component,"diodo");
            break;
        case 'T':
            strcpy(component,"transistor");
            break;
        default:
            strcpy(component,"other");
            
    }
    //printf("El componente es: %s\n",component);
    //printf("La direccion original del componente es: %p\n",component);
    valor_retorno = component ;
    return valor_retorno;
}

void myHandler(int sig)
{
    printf("\n");
    printf("***El programa a finalizado. Saludos***\n");
    exit(0);
}
/*
void myHandler2(int sig)
{
    printf("La cantidad de veces que se busco son: %d\n",cantidad_buscada); 
    
}
*/
int struct2json(PART_NUMBER *elemento,char json[])
{
                    
    
    char string_id[50];
    char string_stock[50];
    char *posicion_string;
    int numero_caracteres;
    char component[20];
    
    posicion_string = typeNumberToTypeWord(elemento->type,component);
                    
    sprintf(string_id,"%d",elemento->id);
    sprintf(string_stock,"%d",elemento->stock);
                                       
                    strcat(json,"{");
                    
                    strcat(json,"\n");
                    strcat(json," ");
                    strcat(json,"\"");
                    strcat(json,"id");
                    strcat(json,"\"");
                    strcat(json,":");
                    strcat(json,string_id);
                    strcat(json,",");
                    strcat(json,"\n");
                
                    strcat(json," ");
                    strcat(json,"\"");
                    strcat(json,"type");
                    strcat(json,"\"");
                    strcat(json,":");
                    strcat(json,component);
                    strcat(json,",");
                    strcat(json,"\n");
                    
                    strcat(json," ");
                    strcat(json,"\"");
                    strcat(json,"description");
                    strcat(json,"\"");
                    strcat(json,":");
                    strcat(json,elemento->description);
                    strcat(json,",");
                    strcat(json,"\n");
                    
                    strcat(json," ");
                    strcat(json,"\"");
                    strcat(json,"sku");
                    strcat(json,"\"");
                    strcat(json,":");
                    strcat(json,elemento->sku);
                    strcat(json,",");
                    strcat(json,"\n");
                    
                    strcat(json," ");
                    strcat(json,"\"");
                    strcat(json,"stock");
                    strcat(json,"\"");
                    strcat(json,":");
                    strcat(json,string_stock);
                    strcat(json,",");
                    strcat(json,"\n");

                    strcat(json,"}");
                    printf("***********************************\n");
                    printf("La posicion del puntero al string type es: %p\n",posicion_string);
                    
                    numero_caracteres = strlen(json);
                    
                    return numero_caracteres;
}
int control_argumentos(int argc,char argv[])
{
    
    char aux_argv[20];
    int num_leido;
    if (argc < 2 || argc > 2)
        {
            printf("La cantidad de argumentos ingresados es diferente a 1!\n");
            return -2;
        }
        
    strcpy(aux_argv,argv);
    num_leido = strlen(aux_argv);
    
    if(aux_argv[num_leido-1]!='t' || aux_argv[num_leido-2]!='x' || aux_argv[num_leido-3]!='t' || aux_argv[num_leido-4]!='.')
    {
        printf("El argumento ingresado no termina en .txt\n");
        return -1;
    }
    return 0;
    
}
void menu_interativo(char argv[])
{
    
    FILE *database;
    PART_NUMBER datos_aux;
    PART_NUMBER *pAElemento[100];
    int dato_retornado;
    int id;
    int opcion;
    char json[1000];
    int valor_return;
    int cantidad_buscada = 0; 
    
    
    printf("\n");
    printf("********Menu de opciones********\n");
    printf("\n");
    printf("*Opcion 1 -> Listar todos los elementos en JSON*\n");
    printf("*Opcion 2 -> Buscar id y mostrarlo en JSON*\n");
    printf("*Finalizar con CTRL+C o mandar una señal SIGINT*\n");
    printf("*Ingresar opcion-> ");
    scanf("%d",&opcion);
    
            if(opcion==1)
            {
                database = fopen(argv,"rb");
                fseek(database,0,SEEK_SET);
                PART_NUMBER aux_struct;
                PART_NUMBER *elemento;
                int cant_caracter_escrito;


                while((fread(&aux_struct,sizeof(PART_NUMBER),1,database))!=0)
                {
                    printf("\n");

                    memset(&json,0,sizeof(json));
                    
                    elemento = (PART_NUMBER*)malloc(sizeof(PART_NUMBER));
                    if(elemento==NULL)
                    {
                        printf("Error en reservar memoria en opcion1\n");
                        exit(-1);
                    }
                    *elemento = aux_struct;
                    
                    cant_caracter_escrito = struct2json(elemento, json);
                    
                    printf("%s",json);
                    printf("\n");
                    printf("La cantidad de caracteres son: %d\n",cant_caracter_escrito);
                    printf("************************************\n");
        
                    free(elemento);
                }
                fclose(database);
            }
            else if(opcion==2)
            {
               
                printf("\n");
                printf("****Buscador de id****\n");
                printf("\n");
                database = fopen(argv,"rb");
                
                printf("Ingresar id a buscar: ");
                scanf("%d",&id);
                datos_aux.id = id;
                
                dato_retornado = getPartNumberByld(database,datos_aux.id,pAElemento);
                
                
                    if(dato_retornado == 0)
                    {
                    //printf("Se encontro el id\n");
                    }
                    else if(dato_retornado == 1)
                    {
                        printf("\nNo se encontro el id\n");
                        
                    }
                    else printf("\nError en el id\n");
                    cantidad_buscada ++;
                    
                }
            else
                {
                    printf("\nError! No ingreso opcion 1 o 2!\n");
                
                }
    
    
}
                    /*
                    printf("el id es: %d\n",aux_struct.id);
                    printf("el type es: %c\n",aux_struct.type);
                    printf("La description es: %s\n",aux_struct.description);
                    printf("el sku es: %s\n",aux_struct.sku);
                    printf("El stock es: %d\n",aux_struct.stock);
                    */
