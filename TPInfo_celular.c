#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>

#define LARGE_ARRAY 50

// -- El puerto donde se conectará, lado servidor --
#define PORT 8010	

// -- máxima cantidad de bytes que puede recibir en una transacción --
#define MAXDATASIZE 4096 
#define BUFFSIZE 1



struct harware_celulares
    {
        int memoria_interna;
        int memoria_ram;
        float CPU_clock;
        float camara;
    };
typedef struct harware_celulares HARW_CEL;

struct base_celulares
{
    char marca[10];
    char modelo[10];
    int codigo_IMEI;
    float peso;

    HARW_CEL hardware;
};

typedef struct base_celulares DT_BASE;

int conectar (char* hostname, int port);
int descarga_archivobin_servidor();
void leer_baseDeDatos(FILE* fp_bin);
int cargar_datos_binarios(FILE* fp_bin,DT_BASE **mem_celular);
void liberar_memoriaDinamica(DT_BASE **mem_data,int contador);
void imprimir_lista_celulares(DT_BASE **mem_celular,int contador);
void guardar_baseDeDatos(DT_BASE **mem_celular,int contador);
void encriptar_baseDeDatos(DT_BASE **mem_celular,int contador);
void desencriptar_baseDeDatos(int desplazamiento,DT_BASE **mem_celular,int contador);
int agregar_registro(DT_BASE **mem_celular,int contador);
int cargar_datos_csv(FILE* fp,DT_BASE **mem_celular,int contador);
FILE* open_file_csv(char *file_name_csv);
void menu_interativo(DT_BASE **mem_celular,int contador);
void buscar_registroIMEI(DT_BASE **mem_celular,int contador);
void exportar_subconjunto_csv(DT_BASE **mem_celular,int *matriz_aux,int indice);
void imprimir_subconjunto(DT_BASE **mem_celular,int *matriz_aux,int contador);
void seleccion_conjunto(DT_BASE **mem_celular,int *matriz_aux,int contador);
void eliminar_registro(DT_BASE **mem_celular,int contador);
void eliminar_subconjunto(DT_BASE **mem_celular,int *matriz_aux,int indice);
void exportar_base_datos(DT_BASE **mem_celular,int contador);

int main(int argc,char **argv)
{
    
    char opcion_cargar_servidor[5];
    printf("\n");
    printf("************Bienvenidos a la base de datos*************\n");
    printf("\n");
    printf("Desea cargar archivo binario de un servidor?\n");
    printf("Ok para cargar/Nok para no cargar: ");
    scanf("%s",opcion_cargar_servidor);
    printf("\n");
    printf("******************************************\n");
    if(strcmp(opcion_cargar_servidor,"Ok") == 0)
    {
        printf("**Se cargara archivo binario desde el servidor\n");
        descarga_archivobin_servidor();
    }
    else printf("**Se cargara archivo binario local\n");
    printf("******************************************\n");
    printf("\n");
    DT_BASE *mem_celular[LARGE_ARRAY];
    FILE *fp_bin;
    FILE *fp_csv;
    int i;

    int contador;

    fp_bin = fopen("database_celulares.bin","rb");
    if(fp_bin == NULL)
    {
        printf("No se pudo abrir el archivo\n");
        exit(-1);
    }
    
    if (argc < 3)
	{
		printf("Falta ingresar mas informacion!\n");
		exit(1);
    }
    

    //leer_baseDeDatos(fp_bin);
    printf("******************************************\n");
    contador = cargar_datos_binarios(fp_bin,mem_celular);
    fclose(fp_bin);

    
    desencriptar_baseDeDatos(atoi(argv[1]),mem_celular,contador);
    //printf("Cantidad de registros del archivo binario: %d\n",contador);
    
    for(i=2;i<argc;i++)
    {
        fp_csv = open_file_csv(argv[i]);
        contador = cargar_datos_csv(fp_csv,mem_celular,contador);
        fclose(fp_csv);
        printf("*Se cargaron con exito los datos del archivo(%d) csv\n",(i-1));
    }
    printf("*Cantidad total de registros cargados: %d\n",contador);
    printf("******************************************\n");

    menu_interativo(mem_celular,contador);
    liberar_memoriaDinamica(mem_celular,contador);
    

    return 0;
}
void encriptar_baseDeDatos(DT_BASE **mem_celular,int contador)
{
    int i;
    int largo_string;
    int j;
    int k;
    char string_original[100];
    char string_encriptado[100];
    int desplazamiento = 5;

    for(i=0;i<contador;i++)
    {
        largo_string = strlen((*mem_celular[i]).marca);
        strcpy(string_original,(*mem_celular[i]).marca);

        for(j=0;j<largo_string;j++)
        {
            if (string_original[j] >='a' && string_original[j]<='z' )
        {

            if (string_original[j] + desplazamiento > 'z')
                string_encriptado[j] = string_original[j]+desplazamiento-'z'+'a';
            else
                string_encriptado[j] = string_original[j]+desplazamiento;

        }
        else
                if (string_original[j] >='A' && string_original[j]<='Z' )
            {

                if (string_original[j] + desplazamiento > 'Z')

                    string_encriptado[j] = string_original[j]+desplazamiento-'Z'+'A';
                else
                    string_encriptado[j] = string_original[j]+desplazamiento;

          }
            else
                string_encriptado[j] = string_original[j];
    }
                string_encriptado [j]='\0';

                strcpy(((*mem_celular[i]).marca),string_encriptado);

                largo_string = strlen((*mem_celular[i]).modelo);
                strcpy(string_original,(*mem_celular[i]).modelo);

        for(k=0;k<largo_string;k++)
        {
            if (string_original[k] >='a' && string_original[k]<='z' )
        {

            if (string_original[k] + desplazamiento > 'z')
                string_encriptado[k] = string_original[k]+desplazamiento-'z'+'a';
            else
                string_encriptado[k] = string_original[k]+desplazamiento;

        }
        else
                if (string_original[k] >='A' && string_original[k]<='Z' )
            {

                if (string_original[k] + desplazamiento > 'Z')

                    string_encriptado[k] = string_original[k]+desplazamiento-'Z'+'A';
                else
                    string_encriptado[k] = string_original[k]+desplazamiento;

          }
            else
                string_encriptado[k] = string_original[k];
    }
    string_encriptado [k]='\0';

         strcpy(((*mem_celular[i]).modelo),string_encriptado);
        }
        printf("***************************************\n");
        printf("Se ha encriptado la base de datos\n");
    }

 void leer_baseDeDatos(FILE* fp_bin)
{
    DT_BASE aux;
    while(fread(&aux,sizeof(DT_BASE),1,fp_bin)!=0)
    {
        printf("la marca es: %s\n",aux.marca);
        printf("El modelo es: %s\n",aux.modelo);
        printf("El codigo IMEI es: %d\n",aux.codigo_IMEI);
        printf("El peso es: %f\n",aux.peso);

        printf("la memoria interna es: %d\n",aux.hardware.memoria_interna);
        printf("la memoria ram es: %d\n",aux.hardware.memoria_ram);
        printf("El CPU clock es: %f\n",aux.hardware.CPU_clock);
        printf("Los Megapicel de la camara es: %f\n",aux.hardware.camara);
        printf("\n");
    }
    printf("-----Se ha imprimido la base de datos-----\n");
}
int cargar_datos_binarios(FILE* fp_bin,DT_BASE **mem_celular)
{
    int contador = 0;
    DT_BASE aux;
    //while(!feof(fp_bin))
    if(fseek(fp_bin,0,SEEK_SET)!=0)
    {
        printf("No se pudo posicionar al principio del archivo\n");
        exit(1);
    }
    int i=0;
    while((fread(&aux,sizeof(DT_BASE),1,fp_bin))!=0)
    {
       mem_celular[i]=(DT_BASE*)malloc(sizeof(DT_BASE));
       if(mem_celular[i]==NULL)
       {
           printf("No se pudo reservar memoria\n");
       }

       *mem_celular[i] = aux;

       i++;
       contador = contador + 1;
    }
    printf("*Se ha cargado con exito el archivo binario\n");
    return contador;
}
void liberar_memoriaDinamica(DT_BASE **mem_data,int contador)
{
    int i;
    for(i=0;i<contador;i++)
    {
        free((mem_data[i]));
    }
    printf("***************************************\n");
    printf("Se libero la memoria dinamica\n");
    printf("***************************************\n");
}
void imprimir_lista_celulares(DT_BASE **mem_celular,int contador)
{
    int i;
    for(i=0;i<contador;i++)
    {
        if((*mem_celular[i]).codigo_IMEI != 0)
        {
        printf("la marca es: %s\n",(*mem_celular[i]).marca);
        printf("El modelo es: %s\n",(*mem_celular[i]).modelo);
        printf("El codigo IMEI es: %d\n",(*mem_celular[i]).codigo_IMEI);
        printf("El peso es: %f\n",(*mem_celular[i]).peso);

        printf("la memoria interna es: %d\n",(*mem_celular[i]).hardware.memoria_interna);
        printf("la memoria ram es: %d\n",(*mem_celular[i]).hardware.memoria_ram);
        printf("El CPU clock es: %f\n",(*mem_celular[i]).hardware.CPU_clock );
        printf("Los Megapicel de la camara es: %f\n",(*mem_celular[i]).hardware.camara);
        printf("\n");
        }
    }
    printf("***************************************\n");
    printf("*Se imprimieron todos los datos\n");
    printf("***************************************\n");
}
void guardar_baseDeDatos(DT_BASE **mem_celular,int contador)
{

    FILE* aux;
    DT_BASE aux2;
    encriptar_baseDeDatos(mem_celular,contador);
    aux = fopen("database_celulares.bin","wb");
    //fseek(aux,0,SEEK_SET);
    int i;
    for(i=0;i<contador;i++)
    {
        if((*mem_celular[i]).codigo_IMEI != 0)
        {
            aux2 = *mem_celular[i];
            fwrite(&aux2,sizeof(struct base_celulares),1,aux);
        }
    }
    
    printf("*Se guardaron todos los datos al archivo binario\n");
    printf("***************************************\n");
    fclose(aux);
}
void desencriptar_baseDeDatos(int desplazamiento,DT_BASE **mem_celular,int contador)
{
    int i;
    int largo_string;
    int j;
    int k;
    char string_desencriptado[100];
    char string_encriptado[100];

    for(i=0;i<contador;i++)
    {
        largo_string = strlen((*mem_celular[i]).marca);
        strcpy(string_encriptado,(*mem_celular[i]).marca);

        for(j=0;j<largo_string;j++)
        {
            if (string_encriptado[j] >='a' && string_encriptado[j]<='z' )
        {

            if (string_encriptado[j] - desplazamiento < 'a')
                string_desencriptado[j] = string_encriptado[j]-desplazamiento + 'z';
            else
                string_desencriptado[j] = string_encriptado[j]-desplazamiento;

        }
        else
                if (string_encriptado[j] >='A' && string_encriptado[j]<='Z' )
            {

                if (string_encriptado[j] - desplazamiento < 'A')

                    string_desencriptado[j] = string_encriptado[j]-desplazamiento+'Z';
                else
                    string_desencriptado[j] = string_encriptado[j]-desplazamiento;

          }
            else
                string_desencriptado[j] = string_encriptado[j];
    }
                string_desencriptado [j]='\0';

                strcpy(((*mem_celular[i]).marca),string_desencriptado);

        largo_string = strlen((*mem_celular[i]).modelo);
        strcpy(string_encriptado,(*mem_celular[i]).modelo);

        for(k=0;k<largo_string;k++)
        {
            if (string_encriptado[k] >='a' && string_encriptado[j]<='z' )
        {

            if (string_encriptado[k] - desplazamiento < 'a')
                string_desencriptado[k] = string_encriptado[k]-desplazamiento + 'z';
            else
                string_desencriptado[k] = string_encriptado[k]-desplazamiento;

        }
        else
                if (string_encriptado[k] >='A' && string_encriptado[k]<='Z' )
            {

                if (string_encriptado[k] - desplazamiento < 'A')

                    string_desencriptado[k] = string_encriptado[k]-desplazamiento+'Z';
                else
                    string_desencriptado[k] = string_encriptado[k]-desplazamiento;

          }
            else
                string_desencriptado[k] = string_encriptado[k];
    }
                string_desencriptado [k]='\0';

                strcpy(((*mem_celular[i]).modelo),string_desencriptado);

        }
        printf("*Se ha desencriptado la base de datos\n");
}
int agregar_registro(DT_BASE **mem_celular,int contador)
{
    DT_BASE aux;
    int i= contador;
    mem_celular[i] = (DT_BASE*)malloc(sizeof(DT_BASE));
    if(mem_celular == NULL)
    {
        printf("No se pudo reservar memoria para el agregado de registro\n");
        exit(-1);
    }
    printf("--Agregar registro--\n");

            printf("la marca es: ");
            scanf("%s",aux.marca);

            printf("El modelo es: ");
            scanf("%s",aux.modelo);

            printf("El codigo IMEI es: ");
            scanf("%d",&aux.codigo_IMEI);

            printf("El peso es: ");
            scanf("%f",&aux.peso);

            printf("la memoria interna es: ");
            scanf("%d",&aux.hardware.memoria_interna);

            printf("la memoria ram es: ");
            scanf("%d",&aux.hardware.memoria_ram);

            printf("El CPU clock es: ");
            scanf("%f",&aux.hardware.CPU_clock );

            printf("Los Megapicel de la camara es: ");
            scanf("%f",&aux.hardware.camara);

            *mem_celular[i] = aux;

            contador = contador+1;
            printf("***************************************\n");
            printf("*Se ha ingresado un nuevo registro\n");
            printf("***************************************\n"); 
            return contador;
            
            
}
int cargar_datos_csv(FILE* fp,DT_BASE **mem_celular,int contador)
{
    DT_BASE aux;
    char leer_linea[200];
    char *token;
    char delimitador[2] = ",";

    //fgets(leer_linea,400,fp);
    //fputs(leer_linea,stdout);

    int i = contador;

    //while(!feof(fp))
    while(fgets(leer_linea,400,fp)!=0)
    {
        //printf("recorre el csv\n");
        //fputs(leer_linea,stdout);

        mem_celular[i] = (DT_BASE*)malloc(sizeof(DT_BASE));

        if(mem_celular[i]==NULL)
        {
            printf("No se pudo reservar memoria\n");
            exit(-1);
        }

        token = strtok(leer_linea,delimitador);
        strcpy(aux.marca,token);

        token = strtok(NULL,delimitador);
        strcpy(aux.modelo,token);

        token = strtok(NULL,delimitador);
        aux.codigo_IMEI = atoi(token);

        token = strtok(NULL,delimitador);
        aux.peso = atof(token);

        token = strtok(NULL,delimitador);
        aux.hardware.memoria_interna= atoi(token);

        token = strtok(NULL,delimitador);
        aux.hardware.memoria_ram = atoi(token);

        token = strtok(NULL,delimitador);
        aux.hardware.CPU_clock = atof(token);

        token = strtok(NULL,delimitador);
        aux.hardware.camara = atof(token);



        *mem_celular[i] = aux;

        i++;
        contador = contador + 1;
    }
    

    return contador;

}
FILE* open_file_csv(char *file_name_csv)
{
    FILE* aux;
    aux = fopen(file_name_csv,"r");
    //printf("Se ha cargado archivo csv\n");

    return aux;
}
void menu_interativo(DT_BASE **mem_celular,int contador)
{
    int opcion = 0;
    int matriz_aux[50];
    int indice = contador;

    while(opcion!=11)
    {
        printf("\n");
        printf("-----------Menu de opciones----------\n");
        printf("\n");
        printf("Opcion 1 Agregar registros\n");
        printf("Opcion 2 buscar registro por IMEI\n");
        printf("Opcion 3 Eliminar registro\n");
        printf("Opcion 4 Seleccion subconjunto por marca o modelo\n");
        printf("Opcion 5 Imprimir subconjunto seleccionado en opcion 4\n");
        printf("Opcion 6 Exportar subconjunto seleccionado en opcion 4 en CSV\n");
        printf("Opcion 7 Eliminar subconjunto seleccionado en opcion 4\n");
        printf("Opcion 8 Imprimir toda la base de datos\n");
        printf("Opcion 9 Exportar toda la base de datos a binario o csv\n");
        printf("Opcion 10 Guardar la base de datos y salir\n");
        printf("Opcion 11 Salir sin guardar\n");
        printf("\n");

        printf("----Ingrese una opcion: ");
        scanf("%d",&opcion);
        printf("\n");


        switch(opcion)
        {
        case 1:

            indice = agregar_registro(mem_celular,indice);
            //printf("La cantidad de registros es: %d\n",indice);

            break;

        case 2:

            buscar_registroIMEI(mem_celular,contador);


            break;
        case 3:

            eliminar_registro(mem_celular,indice);
            
            break;
            
        case 4:
            
            seleccion_conjunto(mem_celular,matriz_aux,contador);
            
            break;
            
        case 5:
            
            imprimir_subconjunto(mem_celular,matriz_aux,indice);
            
            break;
            
            
        case 6:
            exportar_subconjunto_csv(mem_celular,matriz_aux,indice);
            break;
            
        case 7:
            eliminar_subconjunto(mem_celular,matriz_aux,indice);
            break;

        case 8:
            
            imprimir_lista_celulares(mem_celular,indice);
            //printf("El contador es: %d\n",indice);
            break;
            
        case 9:
            exportar_base_datos(mem_celular,indice);
            break;

        case 10:
            //printf("El contador es: %d\n",contador);
            guardar_baseDeDatos(mem_celular,indice);
            opcion = 11;

            break;

        }

    }
}
void buscar_registroIMEI(DT_BASE **mem_celular,int contador)
{
    int numero_IMEI;
    int i;
    printf("--Buscardor de IMEI--\n");
    printf("Ingrese numero de IMEI: ");
    scanf("%d",&numero_IMEI);
    printf("\n");
    
    
    
    for(i=0;i<contador;i++)
    {
        if(numero_IMEI==(*mem_celular[i]).codigo_IMEI)
        
       {

            printf("la marca es: %s\n",(*mem_celular[i]).marca);
            printf("El modelo es: %s\n",(*mem_celular[i]).modelo);
            printf("El codigo IMEI es: %d\n",(*mem_celular[i]).codigo_IMEI);
            printf("El peso es: %f\n",(*mem_celular[i]).peso);

            printf("la memoria interna es: %d\n",(*mem_celular[i]).hardware.memoria_interna);
            printf("la memoria ram es: %d\n",(*mem_celular[i]).hardware.memoria_ram);
            printf("El CPU clock es: %f\n",(*mem_celular[i]).hardware.CPU_clock );
            printf("Los Megapicel de la camara es: %f\n",(*mem_celular[i]).hardware.camara);

       }
    }
}
void exportar_subconjunto_csv(DT_BASE **mem_celular,int *matriz_aux,int indice)
{
    

    char aux_destino_string[1000];
    FILE *fp_exp_csv;
    int i;

    char string_IMEI[20];
    char string_peso[20];
    char string_memoria_interna[20];
    char string_memoria_ram[20];
    char string_clock[20];
    char string_camara[20];
   
    
    memset(&aux_destino_string,0,sizeof(aux_destino_string));


    fp_exp_csv = fopen("subconjunto_csv_exportado.CSV", "w");
    if (fp_exp_csv == NULL)
    {
        printf("Arhivo csv no encontrado\n");
        exit(-1);
    }
    
    for(i=0;i<indice;i++)
        
    {
        if(matriz_aux[i] == 1)
        {
            
            if((*mem_celular[i]).codigo_IMEI != 0)
            {
                sprintf(string_IMEI,"%d",(*mem_celular[i]).codigo_IMEI);
                sprintf(string_peso,"%f",(*mem_celular[i]).peso);
                sprintf(string_memoria_interna,"%d",(*mem_celular[i]).hardware.memoria_interna);
                sprintf(string_memoria_ram,"%d",(*mem_celular[i]).hardware.memoria_ram);
                sprintf(string_clock,"%f",(*mem_celular[i]).hardware.CPU_clock);
                sprintf(string_camara,"%f",(*mem_celular[i]).hardware.camara);
                
                strcat(aux_destino_string,(*mem_celular[i]).marca);
                strcat(aux_destino_string,",");
                strcat(aux_destino_string,(*mem_celular[i]).modelo);
                strcat(aux_destino_string,",");
                strcat(aux_destino_string,string_IMEI);
                strcat(aux_destino_string,",");
                strcat(aux_destino_string,string_peso);
                strcat(aux_destino_string,",");
                strcat(aux_destino_string,string_memoria_interna);
                strcat(aux_destino_string,",");
                strcat(aux_destino_string,string_memoria_ram);
                strcat(aux_destino_string,",");
                strcat(aux_destino_string,string_clock);
                strcat(aux_destino_string,",");
                strcat(aux_destino_string,string_camara);
                strcat(aux_destino_string,"\0");
                strcat(aux_destino_string,"\n");
                
                fseek(fp_exp_csv,0,SEEK_END);
                fputs(aux_destino_string,fp_exp_csv);
                memset(&aux_destino_string,0,sizeof(aux_destino_string));
            
            }
        }


    }

    fclose(fp_exp_csv);
    printf("***************************************\n");
    printf("*Se ha exportado subconjunto a CSV\n");
    printf("***************************************\n");
}
void seleccion_conjunto(DT_BASE **mem_celular,int *matriz_aux,int indice)
{
    int opcion;
    char marca[50];
    char modelo[50];
    int i;
    int j;
    int contador = 0;
    
    for(j=0;j<50;j++)
    {
        matriz_aux[j]=0;
        
    }
    
    
    printf("-----Seleccion de grupos------\n");
    printf("--Opcion 1 seleccion por marca---\n");
    printf("--Opcion 2 seleccion por modelo---\n");
    
    printf("La opcion es: ");
    
    scanf("%d",&opcion);
    
    switch(opcion)
    {
        case 1:
            printf("--Ingresar nombre de la marca a seleccionar: ");
            scanf("%s",marca);
            for(i=0;i<indice;i++)
            {
                if(strcmp(marca,(*mem_celular[i]).marca)==0)
                {
                    matriz_aux[i]=1;
                    contador++;
                }
                //printf("%d\n",matriz_aux[i]);
                
            }
            printf("**************************************\n");
            printf("*La cantidad de registros encontrados son: %d\n",contador);
            printf("**************************************\n");
            
            break;
        case 2:
            printf("--Ingresar nombre del modelo a seleccionar: ");
            scanf("%s",modelo);
            
            for(i=0;i<indice;i++)
            {
                if(strcmp(modelo,(*mem_celular[i]).modelo)==0)
                {
                    matriz_aux[i]=1;
                    contador++;
                }
                //printf("%d\n",matriz_aux[i]);
            }
            printf("**************************************\n");
            printf("*La cantidad de registros encontrados son: %d\n",contador);
            printf("**************************************\n");
            
            break;
        
    }
    
    
}
void imprimir_subconjunto(DT_BASE **mem_celular,int *matriz_aux,int indice)
{
    int i;
    for(i=0;i<indice;i++)
    {
        if(matriz_aux[i] == 1)
        {
            printf("la marca es: %s\n",(*mem_celular[i]).marca);
            printf("El modelo es: %s\n",(*mem_celular[i]).modelo);
            printf("El codigo IMEI es: %d\n",(*mem_celular[i]).codigo_IMEI);
            printf("El peso es: %f\n",(*mem_celular[i]).peso);
            printf("la memoria interna es: %d\n",(*mem_celular[i]).hardware.memoria_interna);
            printf("la memoria ram es: %d\n",(*mem_celular[i]).hardware.memoria_ram);
            printf("El CPU clock es: %f\n",(*mem_celular[i]).hardware.CPU_clock );
            printf("Los Megapicel de la camara es: %f\n",(*mem_celular[i]).hardware.camara);
            printf("\n");
        }
        
        
    }
        
}

int conectar (char* hostname, int port)
{

	int	sockfd;
	struct hostent *he;	            /* Se utiliza para convertir el nombre del host a su dirección IP */
	struct sockaddr_in their_addr;  /* dirección del server donde se conectará */

	// -- convertimos el nombre del host a su dirección IP  --
	if ((he = gethostbyname ((const char *) hostname)) == NULL)
	{
		fprintf(stderr, "Error en Nombre de Host");
		exit(1);
	}
 
    // -- creamos el socket  --
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		perror("Error en creación de socket");
		exit(1);
	}

    // -- establecemos their_addr con la direccion del server --
	their_addr.sin_family = AF_INET;
	their_addr.sin_port = htons(port);
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(their_addr.sin_zero), 8);

    // -- intentamos conectarnos con el servidor  --
	if (connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1)
	{
		fprintf(stderr, "Error tratando de conectar al server");
		exit(1);
	}
	return sockfd;
}
int descarga_archivobin_servidor()
{
    
    int sockfd;  /*File Descriptor para sockets*/
	int numbytes;/*Contendrá el número de bytes recibidos por read () */
	char buf[BUFFSIZE];  /* Buffer donde se reciben los datos de read ()*/
	int filefd;


    // -- conectarse al servidor --
	sockfd = conectar ("127.0.8.1", 8010);
    
    
    filefd = open("./database_celulares.bin",O_WRONLY | O_CREAT,0666);
    
    if(filefd<0)
        {
            fprintf(stderr, "Error en función open %s\n", strerror(sockfd));
		    return -1;
        }
     while(read(sockfd,buf,BUFFSIZE))
     {
         write (filefd, buf , BUFFSIZE); 
                 
     }
    
    // -- devolvemos recursos al sistema --
	close(sockfd);
    close(filefd);

}

void eliminar_registro(DT_BASE **mem_celular,int contador)
{
    
    int numero_IMEI;
    int i;
    printf("--Eliminar registro por IMEI--\n");
    printf("Ingrese numero de IMEI: ");
    scanf("%d",&numero_IMEI);
    printf("\n");

    for(i=0;i<contador;i++)
    {
        if(numero_IMEI==(*mem_celular[i]).codigo_IMEI)
        {
            printf("El codigo IMEI a eliminar es: %d\n",(*mem_celular[i]).codigo_IMEI);
            (*mem_celular[i]).codigo_IMEI = 0;
            
        }

    }
    printf("***************************************\n");
    printf("*Se elimino con exito el registro\n");
    printf("***************************************\n");
    
}
void eliminar_subconjunto(DT_BASE **mem_celular,int *matriz_aux,int indice)
{
    int i;
    for(i=0;i<indice;i++)
    {
        if(matriz_aux[i] == 1)
        {
            (*mem_celular[i]).codigo_IMEI = 0;
        }
        
    }
    printf("***************************************\n");
    printf("*Se elimino subconjunto seleccionado\n");
    printf("***************************************\n");
}
void exportar_base_datos(DT_BASE **mem_celular,int indice)
{
    
    int opcion;
    FILE* aux;
    DT_BASE aux2;
    FILE *fp_exp_csv;
    char aux_destino_string[2000];
    char string_IMEI[20];
    char string_peso[20];
    char string_memoria_interna[20];
    char string_memoria_ram[20];
    char string_clock[20];
    char string_camara[20];
    int i;
    
    
    
    printf("-----Seleccion de grupos------\n");
    printf("Opcion 1 exportar base de datos en formato binario----\n");
    printf("Opcion 2 exportar base de datos en formato csv---\n");
    
    printf("La opcion es: ");
    
    scanf("%d",&opcion);
    
    switch(opcion)
    {
        case 1:
        
            aux = fopen("exp_database_celulares.bin","wb");
            
            int j;
            for(j=0;j<indice;j++)
            {
                if((*mem_celular[j]).codigo_IMEI!=0)
                {
                    aux2 = *mem_celular[j];
                    fwrite(&aux2,sizeof(struct base_celulares),1,aux);
                }
            }
            printf("***************************************\n");
            printf("*Se exportaron todos los datos al archivo binario\n");
            printf("***************************************\n");
            fclose(aux);
            
            break;
        case 2:
        
            memset(&aux_destino_string,0,sizeof(aux_destino_string));


            fp_exp_csv = fopen("base_datos_exp.CSV", "w");
            if (fp_exp_csv == NULL)
            {
                printf("Arhivo csv no encontrado\n");
                exit(-1);
            }
            
            //printf("Se creo el archiv csv exportado\n");
            
            for(i=0;i<indice;i++)
                
            {
                if((*mem_celular[i]).codigo_IMEI!=0)
                    
                {
            
                    sprintf(string_IMEI,"%d",(*mem_celular[i]).codigo_IMEI);
                    sprintf(string_peso,"%f",(*mem_celular[i]).peso);
                    sprintf(string_memoria_interna,"%d",(*mem_celular[i]).hardware.memoria_interna);
                    sprintf(string_memoria_ram,"%d",(*mem_celular[i]).hardware.memoria_ram);
                    sprintf(string_clock,"%f",(*mem_celular[i]).hardware.CPU_clock);
                    sprintf(string_camara,"%f",(*mem_celular[i]).hardware.camara);
                    
                    strcat(aux_destino_string,(*mem_celular[i]).marca);
                    strcat(aux_destino_string,",");
                    strcat(aux_destino_string,(*mem_celular[i]).modelo);
                    strcat(aux_destino_string,",");
                    strcat(aux_destino_string,string_IMEI);
                    strcat(aux_destino_string,",");
                    strcat(aux_destino_string,string_peso);
                    strcat(aux_destino_string,",");
                    strcat(aux_destino_string,string_memoria_interna);
                    strcat(aux_destino_string,",");
                    strcat(aux_destino_string,string_memoria_ram);
                    strcat(aux_destino_string,",");
                    strcat(aux_destino_string,string_clock);
                    strcat(aux_destino_string,",");
                    strcat(aux_destino_string,string_camara);
                    strcat(aux_destino_string,"\0");
                    strcat(aux_destino_string,"\n");
                    
                    fseek(fp_exp_csv,0,SEEK_END);
                    fputs(aux_destino_string,fp_exp_csv);
                    memset(&aux_destino_string,0,sizeof(aux_destino_string));
                }
                    
                }
               printf("***************************************\n");
            fclose(fp_exp_csv);
            
            printf("*Se ha exportado toda la base de datos a CSV\n");
            printf("***************************************\n");
            //printf("El indice es: %d\n",indice);

            break;
        }
}
