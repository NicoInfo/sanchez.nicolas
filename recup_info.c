#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define LARGE 50
#define OK 1
#define ERROR 0

int my_trim(char *str);
int my_uppercase(char *str);
int parser(char *src,char *key,char *value);

int main()
{
    char string_original[LARGE] = "hola como estan";
    char string_mayusculas[LARGE] = "hola mundo";
    int cantidad_espacios;
    int cantidad_conversiones;  
    char string_codigo[LARGE] = "Hardware: 01.11";
    char string_key[LARGE] = "Hardware";
    char *value;
    int resultado;

    cantidad_espacios = my_trim(string_original);
    
    printf("Cantidad de espacios eliminados %d\n",cantidad_espacios);
    
    cantidad_conversiones = my_uppercase(string_mayusculas);
    
    printf("Cantidad de conversiones realizadas: %d\n",cantidad_conversiones);
    
    //printf("Ingresar key a buscar: ");
    //scanf("%s",string_key);
    
    resultado = parser(string_codigo,string_key,value);
    
    if(resultado == OK)
    {
        printf("El key buscado coincide con el string\n");
    }else printf("El key no coincide con el string\n");
    
    
    
 
    return 0;
}
int my_trim(char *string_original)
{
    int largo_string;
    int contador_espacios = 0;
    int i;
    int j;
    
    largo_string = strlen(string_original);
    
    printf("cantidad de elementos: %d\n", largo_string);
    
    for(i=0;i<largo_string;i++)
    {
        if(string_original[i] == 32)
        {
            for(j=i;j<largo_string;j++)
            {
               string_original[j] = string_original[j+1]; 
            }
            
            contador_espacios++;
        }
        
    }
    printf("nuevo string: %s\n",string_original);
    
    return contador_espacios;
    
}
int my_uppercase(char *string_conversion)
{
    int cantidad_conversiones = 0;
    int largo_string;
    int i;
    int j;
    
    
    largo_string = strlen(string_conversion);
    
    for(i=0;i<largo_string;i++)
    {
        if(string_conversion[i] >= 97 && string_conversion[i]<= 122)
        {
            for(j=97;j<123;j++)
            {
                if(string_conversion[i] == j)
                {
                    string_conversion[i] = j-32;
                    
                }
                
                
            }
            
            
          cantidad_conversiones ++;  
        }
        
        
    }
    printf("El nuevo string con mayusculas es: %s\n",string_conversion);
    
    
 
    return cantidad_conversiones;
}
int parser(char *string_data,char *key_buscar,char *value_guardar)
{
    char aux_key[LARGE];
    char aux_value[LARGE];
    int largo_string;
    char *token;
    char delimitador[2] = ":";
    char delimitador2[2] = "\0";
    
    my_uppercase(string_data);
    my_uppercase(key_buscar);
    
    
    token = strtok(string_data,delimitador);
    strcpy(aux_key,token);
    
    token = strtok(NULL,delimitador2);
    strcpy(aux_value,token);
    
    printf("string aux_key: %s\n",aux_key);
    printf("string aux_value: %s\n",aux_value);
    
    if(strcmp(key_buscar,aux_key)==0)
    {
        value_guardar = (char*)malloc(sizeof(aux_key));
        
        value_guardar = aux_key;
        
        printf("El valor de value es: %s\n",value_guardar);
        
        return OK;
        
    }
    else return ERROR;
        
}
