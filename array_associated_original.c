#include<stdio.h>
#define MAX 50
#define TAM 100

int ingresar(char nya[][MAX],int legajo[],int nota[][TAM],int promedio[],int tamano);
void imprimir_promedios(char nya[][MAX],int promedio[],int tamano);
void imprimir_lista_dos_examenes(char nya[][MAX],int nota[][TAM],int tamano);
void imprimir_lista_mejor_promedio(char nya[][MAX],int nota[][TAM],int promedio[],int tamano);
void Imprimir_legajo_cant_notas(int legajo[],int notas[][TAM],int promedio[],int cant_notas[],char nya[][MAX], int estudiantes);
void vaciar_buffer();
int main()
{
    char nombre_apellido[TAM][MAX];
    int legajo[TAM];
    int nota[TAM][TAM];
    int cant_estudiantes;
    int promedio[TAM];
    int cant_notas[TAM];
    cant_estudiantes = ingresar(nombre_apellido,legajo,nota,promedio,TAM);


    imprimir_promedios(nombre_apellido,promedio,cant_estudiantes);
    imprimir_lista_dos_examenes(nombre_apellido,nota,cant_estudiantes);
    imprimir_lista_mejor_promedio(nombre_apellido,nota,promedio,cant_estudiantes);

    Imprimir_legajo_cant_notas(legajo,nota,promedio,cant_notas,nombre_apellido,TAM);

    return 0;
}
int ingresar(char nya[][MAX],int legajo[],int nota[][TAM],int promedio[],int tamano)//Ingresar datos nombre y apellido, legajo,nota. devuelve cantidad de estudiantes.
{
    int i=0;
    int j=0;
    int z=0;
    int valor = 0;
    char respuesta;
    printf("Ingreso de datos****************************************************\n");
    while(1)
    {
        printf("\nIngrese legajo: ");
        scanf(" %d",&legajo[i]);
        vaciar_buffer();

        if(legajo[i] == 0)
        {
            break;
        }
        for(z = 0; z < tamano; z++)
        {
        if(i != z)
          {
              if(legajo[i] == legajo[z])
            {
                while(1)
                {
             printf("********Legajo duplicado,Ingresar otro numero******\n");
             printf("Ingrese legajo: ");
             scanf("%d",&legajo[i]);
             vaciar_buffer();
             if(legajo[i] != legajo[z])
             {
                 break;
             }
             
                }
            }
          }
        }
        printf("Ingresar nombre y apellido: ");
        scanf("%s",nya[i]);
        vaciar_buffer();
        
        while(1)
        {
        printf("Ingrese nota: ");
        scanf(" %d",&nota[i][j]);
        vaciar_buffer();
        
        if(nota[i][j] <= 0 || nota[i][j] > 10)
        {
         while(1)
         {
             printf("*******Ingrese nota entre 0 y 10 inclusive**********\n");
             printf("Ingrese nota: ");
             scanf(" %d",&nota[i][j]);
             vaciar_buffer();
             
             if(nota[i][j] > 0 && nota[i][j] <= 10)
             {
              break;   
             }
         }
        }
        
         valor = valor + nota[i][j];

         j++;
               
        promedio[i] = (valor)/(j);
        
        printf("Desea Ingresar otra nota Y/N.....");
        scanf(" %c",&respuesta);
      
       /* if(!(respuesta == 'y') || respuesta != 'Y' || respuesta != 'n' || respuesta != 'N')
        {
         while(1)
         {
            printf("******* Ingrese una Y para si o una N para no ****..." );
            scanf(" %c",&respuesta);
            
            
            if(respuesta == 'y' || respuesta == 'Y' || respuesta == 'n' || respuesta == 'N')
            {
                break;
                
            }
         }
        }*/
        
        if(respuesta =='N' || respuesta == 'n')
        {
        break;
        }
        
      }
      i++;
      j=0;
      valor=0;
    }
    
    return i;
}
void imprimir_promedios(char nya[][MAX],int promedio[],int tamano)// imprime una lista con los promedios de los estudiantes
{
    printf("\n\n*****************************************************\n");
    printf("\nLista de estudiantes con sus promedios***************************\n");
    int i;
    for(i=0;i<tamano;i++)
    {
        printf("\nNombre y Apellido: %s / Promedio: %d",nya[i],promedio[i]);

    }
}
void imprimir_lista_dos_examenes(char nya[][MAX],int nota[][TAM],int tamano)//Imprime una lista de estudiantes que dieron dos o menos examenes
{
    printf("\n\n*****************************************************\n");
    printf("\nLista de estudiantes que dieron dos o menos examenes***********\n\n");
    int i;
    int j;
    int cuenta = 0;
    for(i=0;i<tamano;i++)
    {
        for(j=0;j<100;j++)
        {
        if(nota[i][j] > 0 && nota[i][j] <= 10)
        {
            cuenta++;
        }
        }
        if(cuenta<=2)
        {
            printf("\nEstudiante en en la posicion %d: %s",i,nya[i]);
        }
        cuenta=0;
        }
    }

void imprimir_lista_mejor_promedio(char nya[][MAX],int nota[][TAM],int promedio[],int tamano)//Imprime una lista de estudiantes con mejores promedios con mas de dos examenes
{
    printf("\n\n*****************************************************\n");
    printf("\nLista de estudiantes que dieron mas de dos examenes con promedios mayores a 7*********\n\n");
    int i;
    int j;
    int cuenta = 0;

    for(i=0;i<tamano;i++)
    {
        for(j=0;j<100;j++)
        {
        if(nota[i][j] > 0 && nota[i][j] <= 10)
        {
            cuenta++;
        }
        }
        if(cuenta>2)
        {
        if(promedio[i] > 7)
           {
            printf("\nEstudiante de la posicion %d  %s / su promedio es %d",i,nya[i],promedio[i]);
           }else
           {
               printf("No existe mejor promedio en la lista");
               
           }
        }
        cuenta=0;
    }
}
void Imprimir_legajo_cant_notas(int legajo[],int nota[][TAM],int promedio[],int cant_notas[],char nya[][MAX],int estudiantes)// imprime legajo,cantidad de notas,promedio
{
    int numero_legajo;
    int i;
    int j;
    char respuesta;
    printf("\n\n*****************************************************\n");
    printf("\n\nNumero de legajo,nombre y apellido, cantidad de notas y promedio*****************\n\n");
    
    while(1)
    {
    printf("Indicar n° de legajo: ");
    scanf("%d",&numero_legajo);
    vaciar_buffer();

    
    for(i=0;i<estudiantes;i++)
    {
        if(legajo[i]== numero_legajo)
        {
            for(j=0;j<estudiantes;j++)
            {
                if(nota[i][j]>0 && nota[i][j]<=10)
                   {
                       cant_notas[i] = cant_notas[i]+1;
                   }
            }
            printf("\nNumero de legajo %d\nNombre y Apellido: %s\nCantidad de notas: %d \nPromedio: %d \n",legajo[i],nya[i],cant_notas[i],promedio[i]);
        }
    }
    printf("Quiere saber otro legajo? Y/N ....  ");
    scanf(" %c",&respuesta);
    vaciar_buffer();
    
    if(respuesta == 'N' || respuesta == 'n')
    {
        break;
    }
    }
}
void vaciar_buffer()
{
    
    int ch;
    while((ch = getchar()) != '\n' && ch != EOF);
    
}
