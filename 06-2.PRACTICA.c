/*
 * Realizar un programa que permita la carga de valores enteros por teclado. 
 * Luego de ingresar el valor mostrar un mensaje por pantalla que pida confirmar 
 * al usuario si desea cargar otro valor ingresando los caracteres 'S' o 'N'. 
 * Mostrar al final la "suma" y el "promedio" de los valores ingresados.
 * Solo se pueden utilizar las siguientes funciones: 
 *      printf
 *      scanf
 *      for/while/do while
 *
 */

#include <stdio.h>


int main(){

    int valores;
    int suma_valores = 0;
    char continuar;
    int i = 0;
    int promedio = 0;
   
    for( ; ; ){
        
        printf("Ingrese un valor: ");
        scanf(" %d", &valores);
        
        suma_valores = suma_valores + valores;
    
        printf("Desea ingresar otro valor S/N: ");
        scanf(" %c",&continuar);
        
         i = i + 1;
        
        promedio = (suma_valores/i);
        
        if(continuar == 'N'){
            break;
        }
        
   }

        printf("La suma total es: %d \n",suma_valores);
        printf("El promedio es %d\n",promedio);
        
  
        return 0;

}