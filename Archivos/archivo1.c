#include <stdio.h>
#include <stdlib.h>


int main()
{
    FILE *fp;
    fp = fopen("datos.txt","rb");
    if(fp == NULL)
    {
        printf("No se pudo abrir el archivo\n");
        exit(1);
    }

    char lectura;
    while(feof(fp)==0)
    {
        lectura = fgetc(fp);
        printf("%c",lectura);
    }

    return 0;
}
