#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE* abrir_archivoBinario(char nombre_archivo[]);
void cargar_datos(FILE* fp);
void cerrar_archivo(FILE* fp);
FILE* abrir_archivo_leer(char *nombre_archivo);
void leer_datos(FILE* fp);
int main()
{
    FILE* fp_bin;
    char nombre_archivo[] = "base.bin";
    fp_bin = abrir_archivoBinario(nombre_archivo);
    
    cargar_datos(fp_bin);
    cerrar_archivo(fp_bin);
    
    fp_bin = abrir_archivo_leer(nombre_archivo);
    leer_datos(fp_bin);
    cerrar_archivo(fp_bin);
    
    
 
    return 0;
}
FILE* abrir_archivoBinario(char *nombre_archivo)
{
 
    FILE* aux;
    aux = fopen(nombre_archivo,"wb");
    
    return aux;
}
void cargar_datos(FILE* fp)
{
    char nombre[15];
    printf("Ingrese nombre: ");
    scanf("%s",nombre);
    
    fwrite(&nombre,sizeof(nombre),1,fp);
    
}
void cerrar_archivo(FILE* fp)
{
    fclose(fp);
}
FILE* abrir_archivo_leer(char *nombre_archivo)
{
    FILE* aux2;
    aux2 = fopen(nombre_archivo,"rb");
    
    return aux2;
}
void leer_datos(FILE* fp)
{
    char aux_leer[20];
    fread(&aux_leer,sizeof(aux_leer),1,fp);
    
    printf("El contenido es: [%s]\n",aux_leer);
}
