#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct base_registro
{
    int id;
    char nombre[15];
};
typedef struct base_registro DT_BASE;

FILE* open_file_write(char *nombre_archivo);
void cargar_datos_escribir(FILE* fp);
void close_file(FILE* fp);
FILE* open_file_read(char *nombre_archivo);
void leer_datos_read(FILE* fp);

int main(int argc,char **argv)
{
    FILE* fp_bin;
    char nombre_archivo[] = "datos_base.bin";
    
    fp_bin = open_file_write(nombre_archivo);
    cargar_datos_escribir(fp_bin);
    close_file(fp_bin);
    
    fp_bin = open_file_read(nombre_archivo);
    leer_datos_read(fp_bin);
    close_file(fp_bin);
 
    return 0;
}
FILE* open_file_write(char *nombre_archivo)
{
    FILE* aux_bin;
    aux_bin = fopen(nombre_archivo,"wb");
    
    return aux_bin;
}
void cargar_datos_escribir(FILE* fp)
{
    DT_BASE aux_write;
    int valores_escrito;
    
    printf("Ingrese el ID: ");
    scanf("%d",&aux_write.id);
    printf("Ingrese nombre: ");
    scanf("%s",aux_write.nombre);
    
    valores_escrito = fwrite(&aux_write,sizeof(aux_write),1,fp);
    printf("Los items escritos son: %d\n",valores_escrito);
    
}
void close_file(FILE* fp)
{
    fclose(fp);
}
FILE* open_file_read(char *nombre_archivo)
{
    FILE* aux_bin;
    aux_bin = fopen(nombre_archivo,"rb");
    
    return aux_bin;
}
void leer_datos_read(FILE* fp)
{
    DT_BASE aux;
    int num_leido;
    num_leido = fread(&aux,sizeof(aux),1,fp);
    
    printf("El ID es: %d\n",aux.id);
    printf("El nombre es: %s\n",aux.nombre);
    printf("Los itens leidos son: %d\n",num_leido);
}
