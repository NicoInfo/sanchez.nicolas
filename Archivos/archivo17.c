#include <stdio.h>
#include <stdlib.h>

struct datos
{
    int id;
    char nombre[15];
};
typedef struct datos DT_BASE;

int main()
{
    FILE* fp;
    fp = fopen("database","rb");
    DT_BASE aux;

    if(fp == NULL)
    {
        printf("No se pudo abrir el archivo\n");
        exit(-1);
    }

    fread(&aux,sizeof(DT_BASE),1,fp);
    while(!feof(fp))
    {
        printf("el id es: %d\n",aux.id);
        printf("el nombre es: %s\n",aux.nombre);
        fread(&aux,sizeof(DT_BASE),1,fp);
    }
    fclose(fp);

    return 0;
}
