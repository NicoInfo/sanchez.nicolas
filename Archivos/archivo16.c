#include <stdio.h>
#include <stdlib.h>

struct datos
{
    int id;
    char nombre[15];
};
typedef struct datos DT_BASE;

int main()
{
    FILE* fp;
    DT_BASE alumno;
    fp = fopen("database","wb");
    if(fp == NULL)
    {
        printf("No se pudo abrir el archivo\n");
    }
    int i;

    for(i=0;i<2;i++)
    {
        printf("Ingresar id: ");
        scanf("%d",&alumno.id);
        printf("Ingresar nombre: ");
        scanf("%s",alumno.nombre);

        fwrite(&alumno,sizeof(DT_BASE),1,fp);
    }

    fclose(fp);


    return 0;
}
