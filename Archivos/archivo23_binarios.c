#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct datos_base
{
    int indice;
    char nombre[20];
};
typedef struct datos_base DT_BASE;

int main()
{
    FILE* fd;
    char archivo_binario[] = "base.bin";
    DT_BASE datos_registro;
    char nombre_aux[20];
    
    fd = fopen(archivo_binario,"wb");
    printf("Ingrese el indice: \n");
    scanf("%d",&datos_registro.indice);
    printf("Ingrese nombre: \n");
    scanf("%s",nombre_aux);
    
    strcpy(datos_registro.nombre,nombre_aux);
    
    fwrite(&datos_registro,sizeof(DT_BASE),1,fd);
    
    printf("Los datos se escribieron correctamente al archivo\n");
    
    fclose(fd);
    
 
    return 0;
}
