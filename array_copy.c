/* Copia un array en
 * otro. 1 en caso
 * de que los tamaños no sean iguales
 */
#include<stdio.h>
#define MAX 3

void array_copy(int a[],int tam1,int c[],int tam2);
void llenar(int a1[],int tam1);
void imprimir(int array1[],int tam1,int array2[],int tam2);
int main()

{ 
    int tam1;
    int tam2;
    printf("Ingrese tamaño de la primera matriz: ");
    scanf(" %d",&tam1);
    printf("Ingrese tamaño de la segunda matriz: ");
    scanf(" %d",&tam2);
    
    int array1[tam1];
    int array2[tam2];
    
    if(tam1==tam2)
    {
    
    llenar(array1,tam1);
    array_copy(array1,tam1,array2,tam2);
    imprimir(array1,tam1,array2,tam2);
    }else{
        printf("No se puede copiar tamaños diferentes -1\n");
    }
    
 return 0;   
}
void llenar(int array1[],int tam1)
{
    int i;
    for(i=0;i<tam1;i++)
    {
        printf("Ingresar el valor de la matriz1 en la posicion %d: ",i);
        scanf(" %d",&array1[i]);
    }
}
void array_copy(int array1[],int tam1,int array2[],int tam2)
{
    int i;
    for(i=0;i<tam1;i++)
    {
        array2[i]=array1[i];
    }
}
void imprimir(int array1[],int tam1, int array2[],int tam2)
{
    int i;
    for(i=0;i<tam1;i++)
    {
        printf("En la posicion %d de la matriz1 es: %d\n",i,array1[i]);
        printf("En la posicion %d de la matriz2 copiada es: %d\n",i,array2[i]);
    }
}
