#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define CANT_HIJOS 3


int main()
{
 
    int i;
    pid_t pid_aux;
    printf("El proceso padre inicial PID: %d\n",getpid());
    
    
    
    for(i=0;i<CANT_HIJOS;i++)
    {
        pid_aux = fork();
        
        if(pid_aux == 0)
        {
         
            printf("El hijo N°%d PID: %d\n",i,getpid());
            exit(-1);
            
        }
        else if(pid_aux > 0)
        {
            printf("El padre PID: %d\n",getpid());
            continue;
            
        }
        else
        {
            printf("Error\n");
            
        }
    }
    while(1)
    {
        sleep(5);
    }
    
 
    return 0;
}
