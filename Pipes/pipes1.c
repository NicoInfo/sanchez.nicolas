#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int valor = 10;
int main()
{
    printf("El proceso padre tiene PID: %d\n",getpid());
    int fd[2];
    int return_pipe;
    pid_t aux_pid;
    char buf[20];
    int num_bytes;
    
    return_pipe = pipe(fd);
    printf("El valor retornado por el pipe es: %d\n", return_pipe);
    
    aux_pid = fork();
    printf("El proceso PID: %d , el aux_pid: %d ejecutandose\n",getpid(),aux_pid);
    
    
    if(aux_pid == 0)//hijo
    {
        printf("El pid del hijo es: %d\n",getpid());
        close(fd[0]);
        write(fd[1],"Hola nico",9);
        write(fd[1],"Hola leo",8);
        close(fd[1]);
        printf("El hijo escribio el mensaje\n");
        valor = 60;
        
    }
    else if(aux_pid > 0)//Padre
    {
        
        printf("El pid del padre es: %d\n",getpid());
        close(fd[1]);
        
        while((num_bytes = read(fd[0],buf,sizeof(buf)))>0)
        {
            buf[num_bytes] = 0;
            printf("El mensaje enviado es: %s\n",buf);
        }
        printf("El leyo el mensaje\n");
        valor = 80;
    }
    else
    {
        printf("No se pudo crear hijos\n");
    }
    while(1)
    {
        sleep(2);
        printf("El pid es: %d y el valor es: %d\n",getpid(),valor);
        
    }
    
    return 0;
}
