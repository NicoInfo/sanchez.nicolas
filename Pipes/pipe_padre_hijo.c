#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()

{
    int fd[2];
    char buf[15];
    int numero_elementos;
    pid_t pidC;
    int return_pipe;
    
    printf("El proceso padre tiene un PID: %d\n",getpid());
    
    return_pipe = pipe(fd);
    if(return_pipe == -1)
    {
     
        printf("Error en la creacion del pipe\n");
        exit(-1);
        
    }
    
    printf("El valor retornado por pipe es: %d\n",return_pipe);
    pidC = fork();
    
    
    
    switch(pidC)
    {
        case 0://hijo
            close(fd[0]);
            write(fd[1],"Hola mundo",10);
            close(fd[1]);
            break;
        case -1:
            printf("Error\n");
            exit(-1);
            break;
        default://padre
            close(fd[1]);
            numero_elementos = read(fd[0],buf,sizeof(buf));
            printf("La cantidad de bytes leidos son: %d y lo escrito por el hijo es: %s\n",numero_elementos,buf);
            close(fd[0]);
    }
    while(1)
    {
        sleep(2);
        printf("El proceso tiene un PID: %d, el PIDC: %d\n",getpid(),pidC);
        
    }
    
    
 
    return 0;
}
