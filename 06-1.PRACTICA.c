/*
 * Realizar un programa que permita la carga de valores enteros por teclado. 
 * Luego de ingresar el valor mostrar un mensaje por pantalla que pida confirmar 
 * al usuario si desea cargar otro valor ingresando los caracteres 'S' o 'N'. 
 * Mostrar al final la suma de los valores ingresados.
 * Solo se pueden utilizar las siguientes funciones: 
 *      printf
 *      scanf
 *      for/while/do while
 *
 * Preguntas:
 *      - ¿Cual de las "estructuras de repeticion" le resulto mas conveniente? probe con la funcion for y me parecio convincente.
 */

#include <stdio.h>

int main(){
    
    int valores;
    int suma_valores = 0;
    char continuar;
   
    for( ; ; ){
        
        printf("Ingrese un valor: ");
        scanf(" %d", &valores);
        
        
        suma_valores = suma_valores + valores;
        
        printf("Desea ingresar otro valor S/N): ");
        scanf(" %c",&continuar);
        
        if(continuar == 'N'){
            break;
        }
        
   }

        printf("La suma total es: %d \n",suma_valores);
  
        return 0;
}
