#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>


int main()
{
    int r;
    int fd;
    char buf[] = "Hola mundo...";
    
    r = mkfifo("./PinPumPam.txt",0666);
    
    if(r==-1)
    {
             printf("Error\n");
    }
    
    fd = open("./PinPumPam.txt",O_WRONLY);
    
    write(fd,buf,sizeof(buf));
    
    close(fd);
    
 
    return 0;
}
